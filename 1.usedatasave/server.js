const express = require('express')
const app = express()
const port = 8000

var bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const _ = require('lodash')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var mongoose = require("mongoose"), timestamps = require('mongoose-timestamp');
const mongooseSerial = require("mongoose-serial")
const crypto = require('crypto');

var path = require('path');
const hbs=require('hbs');

const handlebars = require('express-handlebars');
const { nextTick } = require('process');
//Sets our app to use the handlebars engine
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, "views"));

const connection = mongoose.connect("mongodb://localhost:27017/user-db", { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }  );

var userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required:true,
        trim: true,
        maxlength: 150
    },
    lastname: {
        type: String,
        required:true,
        trim: true,   
        maxlength: 150
    },
    email: {
        type: String,
        required:true,
        trim: true,
        unique:true
    },
    phoneNo: {
        type: Number
    },
    hashedPassword : {
        type: String,
        reuired: true
    },
    yofexp: {
        type: Number,
        default: 0
    },
    skillsets: [
        {
            type: String,
            default: "No skill",
            trim: true
        }
    ],
    current_org: {
        type: String,
        trim: true
    },
    serialnumber: {
        type: String
    },
    timestamp: {
        type: mongoose.Schema.Types.Date,
        default: Date.now,
        immutable: true,
		required: true,
    }
});

userSchema.plugin(mongooseSerial, { field:"serialnumber"});

var User = mongoose.model("User", userSchema);

const getHashedPassword = (password) => {
    console.log("Inside getHashedPassword: ", password)
    const sha256 = crypto.createHash('sha256');
    const hash = sha256.update(password).digest('base64');
    return hash;
}

app.use(bodyParser.json())

app.get("/register", (req,res)=>{
    res.render("register");
});

app.get("/login", (req,res)=>{
res.render("login");
});

app.get("/search", (req,res)=>{
    res.render("search");
    });

app.post("/register", (req, res) => {
    console.log(req.body);
    // Validate request
    if(!req.body) {
        return res.status(400).send({
        message: "Please fill all required field"
        });
    }
    if (req.body.password === req.body.conPassword) {

        let posts = User.find({email:req.body.eMail}, function(err, users){
            console.log("Registration: ", users, users.length)
            if(err){
                console.log(err);
                return;
            }
           if(users.length > 0 ){
               res.render('login', {
                    message: 'User already registered.',
                    messageClass: 'alert-danger'
                });
    
                console.log("ALREADY REGISTERDD");
                return;
            }
            else {
                console.log("NEW REG");
      
               hPassword = getHashedPassword(req.body.password);

               var myskillset = [];
                myskillset.push(req.body.skillset1);
                myskillset.push(req.body.skillset2);
                myskillset.push(req.body.skillset3);

                const phoneno = parseInt(req.body.phno, 10)
                if(Number.isNaN(phoneno)){
                    return res.status(400).json({ error: "Phone number is not a number"})
                }
                
                // Create a new User
               const user = new User({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.eMail,
                phoneNo: phoneno,
                hashedPassword: hPassword,
                yofexp: req.body.yofexp,
                skillsets: myskillset,
                current_org: req.body.curremployer
              });
      
      
            // Save user in the database
             user.save()
             .then(data => {
              res.render('login', {
                  message: 'Registration Successful.  Login to continue.',
                  messageClass: 'alert-success'
                  });
      
               })
             .catch(err => {
                  res.status(500).send({  message: err.message || "Something went wrong while creating new user."}
               );
               });      
          }
        })
    }
})

app.post("/login", (req, res) => {

    // Validate request
    if(!req.body) {
        return res.status(400).send({
        message: "Please fill all required field"
        });
    }

    const { eMail, password } = req.body;

    hPassword=getHashedPassword(req.body.password);

    let posts = User.find({email:req.body.eMail}, function(err, users){
            console.log("Users: ", users)
            console.log("Email: ", req.body.eMail)
            if(err || !users){
                console.log("Error " + err);
                return res.status(401).json({
                    error: "User with that email doesn't exist. Please register before signin."
                })
            }
            else {
                //if(users.length ==0 ) {
                //    res.end("Register before login");
                //}
                if(users[0].hashedPassword === hPassword) {

                    //Generate a token with user id and secret
                    //const token = jwt.sign({_id: users._id}, process.env.JWT_SECRET)
                    //persist the token as 't' in cookie with expiry Date
                    //res.cookie("t", token, {expire: new Date() + 9999})
                    //const {firstname, lastname} = users;
                    //res.json({token, user: {firstname, lastname}})

                    res.render('search', {
                        message: 'Login Successful!!',
                        messageClass: 'alert-success'
                    });

                    //res.redirect('/search');
                }
                else{
                    return res.status(401).json({
                        error: "Email and password doesn't match. Login failed."
                    })
                }
            }
        });

})

app.get('/searchall', function (req, res) {

    var usersProjection = { 
        _id: false
    };
    const s = skillSearch
    const regex = new RegExp(s, 'i') // i for case insensitive

    let posts = User.find({}, usersProjection, function(err, users){

        if(err){
            console.log("Error fetching candidate data " + err);
        }
        else {            
            if(users.length ==0 ) {
                res.end("No candidate found!");
            }else {
                res.json(users);
            }
        }

    });
});

app.post("/search", (req, res) => {

    const { yofExpSearch, skillSearch } = req.body;

    console.log("Garima:Search- ",yofExpSearch,skillSearch)
    
    //If both fields ar blank then return everything
    if(yofExpSearch==0 && skillSearch===""){
        res.redirect("/searchall");
    }

    var usersProjection = { 
        _id: false
    };
    const s = skillSearch
    const regex = new RegExp(s, 'i') // i for case insensitive

    //If years of experience is 0 then check for skill set
    if(yofExpSearch==0 && skillSearch!=""){
        let posts = User.find({skillsets: {$regex: regex}}, usersProjection, function(err, users){

            if(err){
                console.log("Error fetching candidate data " + err);
            }
            else {
                if(users.length ==0 ) {
                    res.end("No candidate found!");
                }else {
                    res.json(users);
                }
            }
    
        });
    }

    //If years of experience is not 0 but skill set is empty
    if(yofExpSearch!=0 && skillSearch===""){
        console.log("Inside 3")
        let posts = User.find({yofexp:yofExpSearch}, usersProjection, function(err, users){

            if(err){
                console.log("Error fetching candidate data " + err);
            }
            else {
                if(users.length ==0 ) {
                    res.end("No candidate found!");
                }else {
                    res.json(users);
                }
            }

        });
    }

    //If both the criteria are provided then use &
    if(yofExpSearch!=0 && skillSearch!=""){
        console.log("Inside 4")
        
        let posts = User.find({yofexp:yofExpSearch, skillsets: {$regex: regex} }, usersProjection, function(err, users){

            if(err){
                console.log("Error fetching candidate data " + err);
            }
            else {
                if(users.length ==0 ) {
                    res.end("No candidate found!");
                }else {
                    res.json(users);
                }
            }

        });
    }

})

app.get("/user/:id", (req, res, id) => {
    console.log("Inside get user", req.params.id, id)
    User.findById(req.params.id).exec((err, user) => {
        console.log("garima: User",user)
        if(err || !user){
            return res.status(400).json({
                error: "User not found"
            })
        }
        req.profile = user;
        req.profile.hashedPassword = undefined;
        return res.json(req.profile)
    })    
})

app.patch("/update/:userId", (req, res, next) => {    
    const id = req.params.userId;
    hPassword = getHashedPassword(req.body.hashedPassword);
    User.update({_id: id}, {$set: {firstname: req.body.firstname, lastname: req.body.lastname, email: req.body.email,
                                   phoneNo: req.body.phoneNo, hashedPassword: hPassword,
                                   yofexp: req.body.yofexp, skillsets: req.body.skillsets, current_org: req.body.current_org}})
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
            error: err
            });
        });
})

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/test.html')
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})